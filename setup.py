from setuptools import setup, find_packages

setup(
    name='calculator',
    version='0.1',
    url="https://bitbucket.org/magnofamily/calculator",
    author="fm",
    author_email="fmagmorg@gmail.com",
    packages=find_packages(),
    scripts=["calculator/device/basic", "calculator/device/scientific",]
)
