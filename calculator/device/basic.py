#!/usr/bin/python

"""
"""
import argparse
from calculator.maths.arithmetic import add, sub, mul, div

operations = {
    "add": add,
    "sub": sub,
    "mul": mul,
    "div": div,
}

parser = argparse.ArgumentParser()
parser.add_argument("op", type=str, choices=operations.keys(), help="Operation: add, sub, mul, div")
parser.add_argument("val1", type=float)
parser.add_argument("val2", type=float)
args = parser.parse_args()


print operations[args.op](args.val1, args.val2),
