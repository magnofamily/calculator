from calculator.maths.arithmetic import *


def test_add():
    assert add(3, 4) == 7

def test_div_zero():
    try:
        div(3.0, 0)
    except ZeroDivisionError:
        assert True
    else:
        assert False

def test_mul_zero():
    assert mul(50000.3, 0.0) == 0

def test_sub():
    assert sub(-5, 400) == -405
